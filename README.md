# pull-request-demo

Even the names of the four Ministries by which we are governed exhibit a sort of impudence 
in their deliberate reversal of the facts. The Ministry of Peace concerns itself with war, 
the Ministry of Truth with lies, the Ministry of Love with torture and the Ministry of Plenty with starvation. 
These contradictions are not accidental, nor do they result from ordinary hypocrisy; they are deliberate exercises in Doublethink. 
For it is only by reconciling contradictions that power can be retained indefinitely. In no other way could the ancient cycle be broken. 
If human equality is to be for ever averted�if the High, as we have called them, are to keep their places permanently�then the prevailing
mental condition must be controlled insanity.